# No 1
Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat.
```java
protected static void registrasi(ArrayList<BcaUser> users) {
        Scanner input = new Scanner(System.in);

        System.out.println();
        System.out.println("Silakan lengkapi informasi berikut untuk membuat akun.");

        System.out.print("Username: ");
        String username = input.next();

        System.out.print("Password: ");
        String password = input.next();

        System.out.print("PIN: ");
        int pin = input.nextInt();

        System.out.print("Saldo awal: ");
        int saldo = input.nextInt();

        System.out.print("Nomor Rekening: ");
        String nomorRekening = input.next();

        BcaUser newUser = new BcaUser(username, password, pin, saldo, nomorRekening);
        users.add(newUser);
        System.out.println("Registrasi berhasil.");
    }
```
Pada class BcaUser, terdapat fungsi yang registrasi yang berguna untuk mendaftarkan diri terleebih dahulu sebelum login.

# No 2
Mampu menjelaskan algoritma dari solusi yang dibuat.

[**BcaApps**](https://gitlab.com/TheRush7/tugas-besar-pbo/-/blob/main/BcaApps.java)

# No 3
Mampu menjelaskan konsep dasar OOP.

Konsep dasar OOP terdiri dari 4 Pilar, yaitu Encapsulation, Abstraction, Inheritance, dan Polymorphism.

1. Encapsulation = membantu mencegah manipulasi data yang tidak valid dan meningkatkan keamanan dari program.
2. Abstraction = membuat kita untuk fokus pada fitur yang lebih penting dan dengan konsep ini juga dapat mengurangi kompleksitas dari program.
3. Inheritance = mengurangi duplikasi kode dan meningkatkan efisiensi pengembangan aplikasi ketika kita membuat kelas-kelas baru yang lebih spesifik dan terfokus.
4. Polymorphism = meningkatkan fleksibilitas program dan memudahkan dalam pengembangan aplikasi yang lebih kompleks.

Penggunaan OOP dalam projek perusahaan dapat mempermudah dalam pengembangan aplikasi baik itu pembuatan maupun pemeliharaan. Selain itu, OOP mempermudah dalam mengembangkan kode-kode kompleks menjadi lebih mudah digunakan, dibaca, dan secara umum lebih baik.

# No 4
Mampu mendemonstrasikan penggunaan Encapsulation secara tepat.
```java
public abstract class BcaApps {
    private static final int transfer = 0;
    protected String username;
    protected String password;
    protected static int pin;
    protected int saldo;
    protected String nomorRekening;
    protected String namaBank;

    public BcaApps(String username, String password, int pin, int saldo, String nomorRekening) {
        this.username = username;
        this.password = password;
        this.pin = pin;
        this.saldo = saldo;
        this.nomorRekening = nomorRekening;
    }

    protected String getUsername() {
        return username;
    }

    protected String getPassword() {
        return password;
    }

    protected String getNomorRekening() {
        return nomorRekening;
    }

    protected int getSaldo() {
        return saldo;
    }

    protected void kurangiSaldo(int jumlah) {
        this.saldo -= jumlah;
    }

    protected void setNomorRekening(String nomorRekening) {
        this.nomorRekening = nomorRekening;
    }

    protected void setUsername(String username) {
        this.username = username;
    }

    protected void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    protected void setPassword(String password) {
        this.password = password;
    }

    protected void setnomorRekening(String nomorRekening) {
        this.nomorRekening = nomorRekening;
    }

    protected boolean cekPin(int pin) {
        return pin == BcaApps.pin;
    }
```
Penggunaan encapsulation di sini sangat berguna, terutama untuk menjaga data saldoRekening agar tidak dapat secara langsung dimanipulasi dari luar.
# No 5
Mampu mendemonstrasikan penggunaan Abstraction secara tepat.
```java
public abstract class BcaApps {
    private static final int transfer = 0;
    protected String username;
    protected String password;
    protected static int pin;
    protected int saldo;
    protected String nomorRekening;
    protected String namaBank;
	
	@Override
    protected void transfer() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan jumlah transfer: ");
        int transferAmount = input.nextInt();

        if (transferAmount > saldo) {
            System.out.println("Saldo tidak mencukupi.");
        } else {
            saldo -= transferAmount;
            System.out.println("Transfer berhasil dilakukan. \nSaldo saat ini: " + saldo);
        }
    }

    @Override
    protected void cekSaldo() {
        System.out.println("Saldo saat ini: " + saldo);
    }

    @Override
    protected void gantiPin() {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan pin lama: ");
        int oldPin = input.nextInt();

        if (cekPin(oldPin)) {
            System.out.println("Masukkan pin baru: ");
            int newPin = input.nextInt();
            System.out.println("Masukkan pin baru lagi: ");
            int newPinConfirmation = input.nextInt();

            if (newPin == newPinConfirmation) {
                pin = newPin;
                System.out.println("Pin berhasil diubah.");
            } else {
                System.out.println("Pin baru tidak sesuai.");
            }
        } else {
            System.out.println("Pin lama salah.");
        }

    }
	
	protected void tambahSaldo(int nominal) {
        saldo += nominal;
        System.out.println("Saldo berhasil ditambahkan. \nSaldo saat ini: " + saldo);
    }
	
	protected abstract void transfer();

    protected abstract void cekSaldo();

    protected abstract void gantiPin();

}
```
Pembuatan abstraction dari class BcaApps di sini dimaksudkan untuk menggambarkan secara umum bagaimana seluruh yang ada di aplikasi bekerja. 
# No 6
Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat.
```java
import java.util.Scanner;
import java.util.ArrayList;

class BcaUser extends BcaApps {
    public BcaUser(String username, String password, int pin, int saldo, String nomorRekening) {
        super(username, password, pin, saldo, nomorRekening);
    }

    @Override
    protected void transfer() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan jumlah transfer: ");
        int transferAmount = input.nextInt();

        if (transferAmount > saldo) {
            System.out.println("Saldo tidak mencukupi.");
        } else {
            saldo -= transferAmount;
            System.out.println("Transfer berhasil dilakukan. \nSaldo saat ini: " + saldo);
        }
    }

    @Override
    public void cekSaldo() {
        System.out.println("Saldo saat ini: " + saldo);
    }

    @Override
    protected void gantiPin() {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan pin lama: ");
        int oldPin = input.nextInt();

        if (cekPin(oldPin)) {
            System.out.println("Masukkan pin baru: ");
            int newPin = input.nextInt();
            System.out.println("Masukkan pin baru lagi: ");
            int newPinConfirmation = input.nextInt();

            if (newPin == newPinConfirmation) {
                pin = newPin;
                System.out.println("Pin berhasil diubah.");
            } else {
                System.out.println("Pin baru tidak sesuai.");
            }
        } else {
            System.out.println("Pin lama salah.");
        }

    }
}
```
Penggunaan inheritance di sini dimaksudkan untuk mempersedikit kode yang dibuat, elemen-elemen pada cekSaldo, gantiPin, dan transfer dirasa hampir mirip dan kedudukan Mutasi lebih tinggi dibanding Aktivitas. Sehingga, saya buat Aktivitas menjadi turunan dari Mutasi dengan sedikit tambahan atribut dan method.

# No 7
Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP?
1. Pertama-tama menganalisis aplikasi lalu membuat list use case dari aplikasi yang akan di-reverse engineering. 
2. Misalnya saya memilih aplikasi BcaApps, sehingga contoh use case-nya "User dapat memasukkan informasi username, password, PIN, saldo awal, nomor rekening". 
3. Setelah itu kita analisis setiap use case dan elemen-elemen apa saja yang dapat/harus dijadikan sebuah objek. 
4. Untuk use case tadi, kita dapat membuat 'transfer' sebagai sebuah fungsi, karena transfer merupakan gambaran umum dari proses bisnis yang terjadi di aplikasi BcaApps. 
5. Abstrak pada aplikasi ini berisi atribut-atribut yang umum seperti transfer, cekSaldo, dan gantiPin akan di-inherit ke kelas 'BcaUser'. 
6. Pada kelas turunan akan lebih dirincikan atribut yang digunakannya, seperti pada 'transfer' ada atribut bank  tujuan, pada 'topuppulsa' ada atribut provider, dan pada 'tagihanbriva' ada atribut institusi. 
7. Lalu untuk modifier-nya, pada kelas turunan kita beri hak akses private untuk setiap atributnya, sedangkan pada kelas abstraknya ada beberapa atribut seperti nominal, biaya admin, dan total biaya yang diberi hak akses protected, agar atribut tersebut dapat diturunkan ke kelas turunan selagi menjaga keamanan data/nilai di dalamnya.

# No 8
Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table
| No.  | Use Case                       | Priority  |
|------|--------------------------------|-----------|
| UC-01| User Login                     | High      |
| UC-02| User Registration              | High      |
| UC-03| Transfer                       | High      |
| UC-04| Check Balance                  | Medium    |
| UC-05| Change PIN                     | Medium    |
| UC-06| Add Balance                    | Medium    |
| UC-07| View Account Number            | Low       |
| UC-08| Payment                        | Medium    |
| UC-09| View Transaction Status        | Low       |
| UC-10| Mutasi Rekening                | Medium    |

link class diagram (```mermaid)
classDiagram
  class BcaApps {
    - username: String
    - password: String
    - pin: static int
    - saldo: int
    - nomorRekening: String
    + BcaApps(username: String, password: String, pin: int, saldo: int, nomorRekening: String)
    + getUsername(): String
    + getPassword(): String
    + getNomorRekening(): String
    + getSaldo(): int
    + kurangiSaldo(jumlah: int): void
    + setNomorRekening(nomorRekening: String): void
    + setUsername(username: String): void
    + setSaldo(saldo: int): void
    + setPassword(password: String): void
    + cekPin(pin: int): boolean
    + transfer(): void
    + cekSaldo(): void
    + gantiPin(): void
    + tambahSaldo(nominal: int): void
  }

  class BcaUser {
    - statusTransaksi: String
    + BcaUser(username: String, password: String, pin: int, saldo: int, nomorRekening: String)
    + getStatusTransaksi(): String
    + setStatusTransaksi(statusTransaksi: String, transferAmount: int): void
    + transfer(): void
    + cekSaldo(): void
    + gantiPin(): void
    + mutasiRekening(): void
    + cekStatusTransaksi(berhasil: boolean): void
  }

  BcaApps <|-- BcaUser

# No 9
Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video

[![Demo Projek: Reverse Engineering Aplikasi Mobile "BCA Mobile"](https://drive.google.com/drive/folders/1Jrx5E0iif02Mkoey9Dk9Kfgw1klb3_Oc?usp=drive_link)

# No 10
Inovasi UX

    } else if (menu == 7) {                        
    System.out.println("==============================");
    System.out.println("       Mutasi Rekening        ");
    System.out.println("==============================");

    // Menampilkan mutasi rekening
    currentUser.mutasiRekening();
    } else if (menu == 8) {
    System.out.println("==============================");
    System.out.println("       Status Transaksi       ");
    System.out.println("==============================");

    // mengecek status transaksi
    currentUser.cekStatusTransaksi(false);

                        } else {
                            System.out.println("Menu tidak valid.");
                        }
