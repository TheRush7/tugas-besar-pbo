import java.util.Scanner;
import java,util.ArrayList;

public abstract class BcaApps {
    private static final int TRANSFER = 0;
    protected String username;
    protected String password;
    protected static int pin;
    protected int saldo;
    protected String nomorRekening;
    protected String namaBank;

    public BcaApps(String username, String password, int pin, int saldo, String nomorRekening) {
        this.username = username;
        this.password = password;
        BcaApps.pin = pin;
        this.saldo = saldo;
        this.nomorRekening = nomorRekening;
    }

    protected String getUsername() {
        return username;
    }

    protected String getPassword() {
        return password;
    }

    protected String getNomorRekening() {
        return nomorRekening;
    }

    protected int getSaldo() {
        return saldo;
    }

    protected void kurangiSaldo(int jumlah) {
        this.saldo -= jumlah;
    }

    protected void setNomorRekening(String nomorRekening) {
        this.nomorRekening = nomorRekening;
    }

    protected void setUsername(String username) {
        this.username = username;
    }

    protected void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    protected void setPassword(String password) {
        this.password = password;
    }

    protected boolean cekPin(int pin) {
        return pin == BcaApps.pin;
    }

    protected abstract void transfer();

    protected abstract void cekSaldo();

    protected abstract void gantiPin();

    protected void tambahSaldo(int nominal) {
        saldo += nominal;
        System.out.println("Saldo berhasil ditambahkan. \nSaldo saat ini: " + saldo);
    }
}

class BcaUser extends BcaApps {
    public BcaUser(String username, String password, int pin, int saldo, String nomorRekening) {
        super(username, password, pin, saldo, nomorRekening);
    }

    @Override
    protected void transfer() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan jumlah transfer: ");
        int transferAmount = input.nextInt();

        if (transferAmount > saldo) {
            System.out.println("Saldo tidak mencukupi.");
        } else {
            saldo -= transferAmount;
            System.out.println("Transfer berhasil dilakukan. \nSaldo saat ini: " + saldo);
        }
    }

    @Override
    protected void cekSaldo() {
        System.out.println("Saldo saat ini: " + saldo);
    }

    @Override
    protected void gantiPin() {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan pin lama: ");
        int oldPin = input.nextInt();

        if (cekPin(oldPin)) {
            System.out.println("Masukkan pin baru: ");
            int newPin = input.nextInt();
            System.out.println("Masukkan pin baru lagi: ");
            int newPinConfirmation = input.nextInt();

            if (newPin == newPinConfirmation) {
                pin = newPin;
                System.out.println("Pin berhasil diubah.");
            } else {
                System.out.println("Pin baru tidak sesuai.");
            }
        } else {
            System.out.println("Pin lama salah.");
        }
    }
}
