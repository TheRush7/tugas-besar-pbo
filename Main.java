import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("===============================");
        System.out.println(" Selamat datang di BCA MOBILE ");
        System.out.println("===============================");

        Scanner input = new Scanner(System.in);
        Scanner str = new Scanner(System.in);
        ArrayList<BcaUser> users = new ArrayList<BcaUser>();

        BcaUser user1 = new BcaUser("arif24", "arifin24", 123456, 1000000, "124314134");
        users.add(user1);

        while (true) {
            System.out.println("===============================");
            System.out.println("      Silakan pilih menu       ");
            System.out.println("===============================");
            System.out.println("1. Login");
            System.out.println("2. Registrasi");
            System.out.println("0. Keluar");

            System.out.print("Pilihan: ");
            int menuUtama = input.nextInt();

            if (menuUtama == 0) {
                System.out.println("Terima kasih telah menggunakan BCA Mobile.");
                break;
            } else if (menuUtama == 2) {
                registrasi(users);
            } else if (menuUtama == 1) {
                BcaUser currentUser = login(users);

                if (currentUser != null) {
                    while (true) {
                        System.out.println("===============================");
                        System.out.println("      Silakan pilih menu       ");
                        System.out.println("===============================");
                        System.out.println("1. Transfer");
                        System.out.println("2. Cek Saldo");
                        System.out.println("3. Ganti Pin");
                        System.out.println("4. Tambah Saldo");
                        System.out.println("5. Nomor Rekening");
                        System.out.println("6. Payment");
                        System.out.println("0. Kembali ke Menu Utama");

                        System.out.print("Pilihan: ");
                        int menu = input.nextInt();

                        if (menu == 0) {
                            break;
                        } else if (menu == 1) {
                            System.out.println("===============================");
                            System.out.println("      Silakan pilih menu       ");
                            System.out.println("===============================");
                            System.out.println("1. Virtual Account");
                            System.out.println("2. Sakuku");
                            System.out.println("3. Transfer antar bank");
                            System.out.println("4. Transfer antar rekening");
                            System.out.print("Pilihan: ");

                            int subMenu1 = input.nextInt();

                            if (subMenu1 == 1) {

                                System.out.println("================================");
                                System.out.println("   Pembayaran Virtual Account   ");
                                System.out.println("================================");
                                System.out.print("Masukkan nama Virtual Account: ");
                                String namaVA = str.nextLine();

                                System.out.println("Nama: " + namaVA);

                                // Lakukan transfer ke Virtual Account
                                currentUser.transfer();
                            } else if (subMenu1 == 2) {
                                System.out.println("=============================");
                                System.out.println("      Pembayaran Sakuku      ");
                                System.out.println("=============================");
                                System.out.print("Masukkan nama Sakuku: ");

                                String namaSakuku = str.nextLine();
                                System.out.println("Nama: " + namaSakuku);

                                // Lakukan transfer ke Sakuku
                                currentUser.transfer();
                            } else if (subMenu1 == 3) {
                                System.out.println("===============================");
                                System.out.println("       Transfer ke Bank        ");
                                System.out.println("===============================");
                                System.out.print("Masukkan nama bank tujuan: ");
                                String namaBank = str.nextLine();
                                System.out.print("Masukkan nama rekening tujuan: ");
                                String namaRek = str.nextLine();
                                System.out.print("Masukkan jumlah transfer: ");
                                int jumlahTransfer = input.nextInt();
                                int biaya = 2500;
                                int totalTransfer = jumlahTransfer + biaya;
                                // Tampilkan detail transfer antar bank
                                System.out.println("Nama Bank: " + namaBank);
                                System.out.println("Nama Rekening: " + namaRek);
                                System.out.println("Jumlah Transfer: " + jumlahTransfer);
                                System.out.println("Biaya: " + biaya);
                                System.out.println("Total Transfer: " + totalTransfer);
                                System.out.println("===============================");
                                // Lakukan transfer ke Bank
                                currentUser.transfer();

                            } else if (subMenu1 == 4) {
                                System.out.println("===============================");
                                System.out.println("    Transfer antar Rekening    ");
                                System.out.println("===============================");
                                System.out.print("Masukkan nomor rekening tujuan: ");
                                String nomorRek = input.next();
                                System.out.print("Masukkan nama pemilik rekening: ");
                                String namaRek = str.nextLine();
                                // Tampilkan detail transfer antar rekening
                                System.out.println("Nomor Rekening Tujuan: " + nomorRek);
                                System.out.println("Nama Pemilik Rekening: " + namaRek);
                                System.out.print("Konfirmasi transfer (y/n)? ");
                                String konfirmasi = input.next();
                                if (konfirmasi.equals("y")) {
                                    // Lakukan transfer antar rekening
                                    currentUser.transfer();
                                }
                            }

                        } else if (menu == 2) {
                            currentUser.cekSaldo();
                        } else if (menu == 3) {
                            currentUser.gantiPin();
                        } else if (menu == 4) {
                            System.out.print("Masukkan jumlah saldo yang ingin ditambahkan: ");
                            int tambahSaldo = input.nextInt();
                            currentUser.tambahSaldo(tambahSaldo);
                        } else if (menu == 5) {
                            System.out.println("Nomor Rekening Anda adalah " + currentUser.getNomorRekening());
                        } else if (menu == 6) {
                            System.out.println("===========================");
                            System.out.println("         Payment           ");
                            System.out.println("===========================");
                            System.out.println("1. Kartu Kredit");
                            System.out.println("2. Handphone");
                            System.out.println("3. Telephone");
                            System.out.println("4. Publik/utilitas");
                            System.out.println("5. BPJS");
                            System.out.println("6. Pajak");
                            System.out.println("7. Asuransi");
                            System.out.println("8. Internet");
                            System.out.println("9. Pinjaman");
                            System.out.println("Pilihan: ");

                            int subMenu2 = input.nextInt();

                            if (subMenu2 == 1) {

                                System.out.println("===============================");
                                System.out.println("    Pembayaran Kartu Kredit    ");
                                System.out.println("===============================");

                                System.out.println("Masukkan Nama Bank: ");
                                String namaBank = str.nextLine();

                                System.out.println("Masukkan No Kartu Kredit: ");
                                String NokartuKredit = str.next();

                                System.out.println("Nama Bank: " + namaBank);
                                System.out.println("No Kartu Kredit: " + NokartuKredit);

                                currentUser.transfer();

                            } else if (subMenu2 == 2) {
                                System.out.println("================================");
                                System.out.println("      Pembayaran Handphone      ");
                                System.out.println("================================");
                                System.out.println("Masukkan Operator Handphone: ");
                                String OperatorHandphone = str.next();
                                System.out.println("Masukkan No HP: ");
                                String NoHp = str.next();

                                System.out.println("Operator Handphone: " + OperatorHandphone);
                                System.out.println("No Handphone: " + NoHp);
                                currentUser.transfer();

                            } else if (subMenu2 == 3) {
                                System.out.println("================================");
                                System.out.println("      Pembayaran Telephone      ");
                                System.out.println("================================");
                                System.out.println("Masukkan Operator Telephone: ");
                                String OperatorTelephone = input.next();
                                System.out.println("Masukkan No Telephone: ");
                                String NoTelephone = str.next();
                                System.out.println("Masukkan Kode Area:");
                                String KodeArea = str.next();

                                System.out.println("Operator Telephone: " + OperatorTelephone);
                                System.out.println("No Telephone: " + NoTelephone);
                                System.out.println("Kode Area: " + KodeArea);
                                currentUser.transfer();

                            } else if (subMenu2 == 4) {
                                System.out.println("===============================");
                                System.out.println("      Pembayaran Utilitas      ");
                                System.out.println("===============================");
                                System.out.println("Masukkan Nama Perusahaan: ");
                                String namaPerusahaan = str.nextLine();
                                System.out.println("Masukkan No Bayar: ");
                                String noBayar = input.next();
                                System.out.println("Masukkan Wilayah: ");
                                String namaWilayah = str.next();
                                System.out.println("Masukkan Jenis PAM: ");
                                String jenisPAM = str.next();

                                System.out.println("Perusahaan: " + namaPerusahaan);
                                System.out.println("No: " + noBayar);
                                System.out.println("Wilayah: " + namaWilayah);
                                System.out.println("Jenis PAM: " + jenisPAM);
                                currentUser.transfer();

                            } else if (subMenu2 == 5) {
                                System.out.println("==============================");
                                System.out.println("       Pembayaran BPJS        ");
                                System.out.println("==============================");
                                System.out.println(
                                        "=====Catatan===== \nUntuk pembayaran BPJS Kesehatan, pastikan nomor bayar terdiri dari 16 angka (88888 + 11 digit akhir No. Kartu Kredit)");
                                System.out.print("Masukkan Jenis BPJS: ");
                                String jenisBPJS = str.nextLine();
                                System.out.print("Masukkan No Bayar: ");
                                String noBPJS = str.next();

                                System.out.println("Jenis BPJS: " + jenisBPJS);
                                System.out.println("No: " + noBPJS);
                                currentUser.transfer();

                            } else if (subMenu2 == 6) {
                                System.out.println("==============================");
                                System.out.println("       Pembayaran Pajak       ");
                                System.out.println("==============================");
                                System.out.println("Masukkan Nama Perusahaan: ");
                                String namaPerusahaan = str.nextLine();
                                System.out.println("Masukkan Kode Bayar: ");
                                String kodeBayar = str.next();

                                System.out.println("Perusahaan: " + namaPerusahaan);
                                System.out.println("Kode Bayar: " + kodeBayar);
                                currentUser.transfer();

                            } else if (subMenu2 == 7) {
                                System.out.println("===============================");
                                System.out.println("      Pembayaran Asuransi      ");
                                System.out.println("===============================");

                                System.out.println("Masukkan nama perusahaan: ");
                                String namaPerusahaan = str.nextLine();
                                System.out.println("Masukkan No.Polis: ");
                                String noPolis = str.next();

                                System.out.println(
                                        "------------------ \n      Catatan \n------------------ \nKode bayar: \n101 - Premi Pertama \n102 - Premi Lanjutan \n103 - Pemulihan \n104 - Topup \n105 - Perubahan \n106 - Cetak Polis");

                                System.out.println("Perusahaan: " + namaPerusahaan);
                                System.out.println("No. Polis: " + noPolis);

                                // Melakukan Transfer
                                currentUser.transfer();

                            } else if (subMenu2 == 8) {
                                System.out.println("==============================");
                                System.out.println("      Pembayaran Internet     ");
                                System.out.println("==============================");

                                System.out.println("Masukkan Nama Perusahaan: ");
                                String perusahaanNama = str.nextLine();
                                System.out.println("Masukkan No Pelanggan: ");
                                String noPelanggan = str.next();

                                System.out.println("Perusahaan: " + perusahaanNama);
                                System.out.println("No Pelanggan:" + noPelanggan);
                                currentUser.transfer();

                            } else if (subMenu2 == 9) {
                                System.out.println("==============================");
                                System.out.println("      Pembayaran Pinjaman     ");
                                System.out.println("==============================");

                                System.out.println("Masukkan Nama Perusahaan: ");
                                String NamaPerusahaan = str.nextLine();
                                System.out.println("Masukkan No Pelanggan: ");
                                String NomorPelanggan = str.next();

                                System.out.println("Perusahaan: " + NamaPerusahaan);
                                System.out.println("No pelanggan: " + NomorPelanggan);
                                currentUser.transfer();

                            }

                        } else {
                            System.out.println("Menu tidak valid.");
                        }
                    }
                }
            }
        }
    }

    protected static void registrasi(ArrayList<BcaUser> users) {
        Scanner input = new Scanner(System.in);

        System.out.println();
        System.out.println("Silakan lengkapi informasi berikut untuk membuat akun.");

        System.out.print("Username: ");
        String username = input.next();

        System.out.print("Password: ");
        String password = input.next();

        System.out.print("PIN: ");
        int pin = input.nextInt();

        System.out.print("Saldo awal: ");
        int saldo = input.nextInt();

        System.out.print("Nomor Rekening: ");
        String nomorRekening = input.next();

        BcaUser newUser = new BcaUser(username, password, pin, saldo, nomorRekening);
        users.add(newUser);
        System.out.println("Registrasi berhasil.");
    }

    protected static BcaUser login(ArrayList<BcaUser> users) {
        Scanner input = new Scanner(System.in);

        System.out.print("Username: ");
        String username = input.next();
        System.out.print("Password: ");
        String password = input.next();

        for (BcaUser user : users) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                System.out.println("Login berhasil.");
                return user;
            }
        }

        System.out.println("Maaf, username atau password yang Anda masukkan salah.");
        return null;
    }

}
